import requests
import json
from .keys import PEXELS_API_KEY as pex_key
from .keys import OPEN_WEATHER_API_KEY as weather_key

def pex_api(city, state):
    call = requests.get(f"https://api.pexels.com/v1/search?query={city} {state}&size=medium&page=1&per_page=1", headers={"Authorization": pex_key})
    r_dict = json.loads(call.content)
    print(r_dict)
    url = r_dict["photos"][0]["url"]
    return url

def weather_api(location_object):
    try:
        call = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={location_object.city},{location_object.state},US&limit=5&appid={weather_key}")
        r_dict = json.loads(call.content)
        coords = {"lat": r_dict[0]["lat"], "lon": r_dict[0]["lon"]}
        call = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat={coords['lat']}&lon={coords['lon']}&appid={weather_key}&units=imperial")
        r_dict = json.loads(call.content)
        weather = {
            "temp": r_dict["main"]["temp"],
            "description": r_dict["weather"][0]["description"]
        }
    except:
        weather = None
    return weather
